all:
	$(MAKE) -C mjpg-streamer-experimental/ $@

# delegate to subdir
%:
	$(MAKE) -C mjpg-streamer-experimental/ $@

# This can't be the default target because it creates a loop when dh
# calls the first target in this makefile.
deb:
	gbp buildpackage  --git-ignore-branch --git-upstream-tree=HEAD -us -uc
